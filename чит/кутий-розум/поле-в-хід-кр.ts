import { appendFileSync, writeFileSync } from "node:fs";
import tf from "./tf";
import { БгалКР, УявнийКутийРозум } from "./уявний-кр";
import { ДаніКРСлужба } from "./дані-служба";
import { однобіжно, перемішати } from "../обробні";
import { ручитиИснуванняПутіФайлаОБ } from "../файловий-сустрій";
import { Куб, ПоворотСторони } from "../гра/куб";
import { Гра } from "../гра/гра";
import { спитКР } from "../гра/спит";

type ВхДані = Куб;
const кількістьПоворотівСторони = 1
type ВихДані = [ПоворотСторони | null];

type ТварВхДаних = tf.Rank.R4;
type ТварВихДаних = tf.Rank.R1;

type ВхДаніКР = tf.Tensor<ТварВхДаних>;
type ВихДаніКР = tf.Tensor<ТварВихДаних>;

export class ПолеВХідКутийРозум extends УявнийКутийРозум<
  ВхДані,
  ВихДані,
  ВхДаніКР,
  ВихДаніКР
> {
  readonly розмірВхіднихДаних: number;
  readonly тварВхіднихДаних: tf.ShapeMap[ТварВхДаних];
  readonly розмірВихіднихДаних: number;
  readonly тварВихіднихДаних: tf.ShapeMap[ТварВихДаних];
  readonly пострійЧовпення: Partial<
    tf.ModelFitArgs & tf.ModelFitDatasetArgs<{ xs: tf.Tensor; ys: tf.Tensor }>
  >;

  constructor(
    private readonly даніКРСлужба: ДаніКРСлужба,
    public readonly розмірність: number,
    verbose: 0 | 1 = 1,
  ) {
    super();

    this.пострійЧовпення = {
      verbose,
    };

    this.розмірВхіднихДаних = даніКРСлужба.кубДані(this.розмірність).розмір;
    this.тварВхіднихДаних = даніКРСлужба.кубДані(this.розмірність).твар;

    this.розмірВихіднихДаних = даніКРСлужба.поворотиСторониДані(кількістьПоворотівСторони).розмір;
    this.тварВихіднихДаних = даніКРСлужба.поворотиСторониДані(кількістьПоворотівСторони).твар;
  }

  створитиБгал() {
    const бгал = tf.sequential();
    бгал.add(
      tf.layers.inputLayer({
        inputShape: this.тварВхіднихДаних,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: [this.розмірВхіднихДаних],
      }),
    );
    // бгал.add(
    //   tf.layers.dense({
    //     units: Math.round(this.розмірВхіднихДаних),
    //     activation: "relu",
    //   }),
    // );
    // бгал.add(
    //   tf.layers.dropout({ rate: 0.2 }),
    // );
    // бгал.add(
    //   tf.layers.dense({
    //     units: Math.round(this.розмірВхіднихДаних / 2),
    //     activation: "relu",
    //   }),
    // );
    // бгал.add(
    //   tf.layers.dense({
    //     units: Math.round(this.розмірВхіднихДаних / 2),
    //     activation: "selu",
    //   }),
    // );
    бгал.add(
      tf.layers.dense({
        units: Math.round(this.розмірВихіднихДаних * 4),
        activation: "selu",
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: Math.round(this.розмірВихіднихДаних * 4),
        activation: "selu",
      }),
    );
    бгал.add(
      tf.layers.dense({
        units: this.розмірВихіднихДаних,
        activation: `sigmoid`,
      }),
    );
    бгал.add(
      tf.layers.reshape({
        targetShape: this.тварВихіднихДаних,
      }),
    );

    this.укласти(бгал);

    return бгал;
  }

  private укласти(бгал: БгалКР) {
    // бгал.compile({ loss: 'meanSquaredError', optimizer: 'sgd' });
    бгал.compile({
      // loss: (исть: tf.Tensor, пит: tf.Tensor) => {
      //   исть.print()
      //   console.log(исть.shape)
      //   // пит.print()
      //   // console.log(пит.shape)
      //   // исть.sub(исть.mul(пит)).print()

      //   return tf.sum(исть.sub(исть.mul(пит)), -1);
      // },
      // loss: "meanSquaredError",
      // loss: tf.losses.absoluteDifference,
      loss: tf.losses.huberLoss,
      // loss: tf.losses.sigmoidCrossEntropy,
      // optimizer: "sgd",
      // optimizer: tf.train.adadelta(0.01),
      optimizer: tf.train.adamax(0.01),
      // optimizer: tf.train.adagrad(0.001),
      // optimizer: tf.train.momentum(0.01, 0.9, true),
      // optimizer: tf.train.rmsprop(0.01),
      metrics: [
        // "accuracy",
        // tf.metrics.categoricalAccuracy,
        // tf.metrics.categoricalCrossentropy,
        // tf.metrics.binaryCrossentropy,
        // tf.metrics.binaryAccuracy,
        // function точність(исть: tf.Tensor, пит: tf.Tensor) {
        //   return tf.logicalAnd(
        //     исть.mul(пит).max(1).max(1).equal(пит.max(1).max(1)),
        //     пит.max(1).max(1).notEqual(0),
        //   );
        // },
      ],
    });
  }

  async створитиЧовпніДаніКР(): Promise<void> {
    ручитиИснуванняПутіФайлаОБ("./кутий-розум-човпне/човпні-дані.json");
    writeFileSync(
      "./кутий-розум-човпне/човпні-дані.json",
      JSON.stringify(await this.витягПоворотів()),
    );
  }

  private витягПоворотів() {
    const гра = new Гра(this.розмірність);
    const куб = гра.створитиКуб();
    const ратіСт = гра.жеребіРаті(1, 10);
    const повороти = Гра.раті(ратіСт)
    const дані: [ВхДані, ВихДані][] = повороти.map((п, вк) => {
      куб.повернути(п);
      const зворотніПовороти: ПоворотСторони[] = повороти.slice(0, вк + 1).reverse().slice(0, кількістьПоворотівСторони).map(п => {
        return { ...п, годинниково: !п.годинниково }
      })
      return [куб.сколок(), Array(кількістьПоворотівСторони).fill(null).map((_, вк) => зворотніПовороти[вк] ?? null) as [ПоворотСторони | null]];
    });

    return дані;
  }

  async витягЧовпнийНабірДанихКР() {
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const that = this;

    // const data = await that.витягПоворотів();
    const data = null;
    // const data = JSON.parse(
    //   readFileSync(
    //     "./кутий-розум-човпне/човпні-дані-ймовірні-ходи.json",
    //     "utf-8",
    //   ),
    // ) as Awaited<ReturnType<typeof that.витягПоворотів>>;

    const створювач = async function* () {
      let повороти = data ?? (await that.витягПоворотів());
      for (let i = 0; i < 2; i++) {
        if (повороти.length === 0) {
          console.error("Помилка");
        }
        for (const дані of повороти) {
          yield {
            xs: await that.вВхДаніКР(дані[0]),
            ys: await that.вВихДаніКР(дані[1]),
          };
        }

        if (i === 1) {
          i = 0;
          повороти = data ?? (await that.витягПоворотів());
        }
      }
    } as unknown as () => Generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>;

    return tf.data
      .generator<{ xs: ВхДаніКР; ys: ВихДаніКР }>(створювач)
      .batch(32 * 2) as tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>;
  }

  async витягЧовпнийРядДанихКР() {
    const дані = await this.витягПоворотів();
    const човпніДані = await однобіжно(дані, async (д) => {
      return [await this.вВхДаніКР(д[0]), await this.вВихДаніКР(д[1])] as const;
    });

    return {
      многоВхДаніКР: човпніДані.map((д) => д[0]),
      многоВихДаніКР: човпніДані.map((д) => д[1]),
    };
  }

  async човпитиВивійно(бгал: БгалКР, оцінювач?: (куб: Куб) => Promise<number> | number) {
    console.log("Початок вивію");
    writeFileSync("./тимч/вивід.txt", "");
    let перемог = 0;
    let поразок = 0;
    const наймКст = 3;
    const найбКст = 3;
    for (let і = 1; і <= 1000; і++) {
      if (await this.човпитиПоГрі(бгал, наймКст, найбКст, оцінювач ?? Куб.оцінкаСтану)) перемог++;
      else поразок++;

      const межаПровірки = 200
      if (і % межаПровірки === 0) {
        const спитів = Math.round(межаПровірки / 10)
        const зібраних = await спитКР((куб) => this.віщувати(бгал, куб), спитів, this.розмірність, наймКст, найбКст);

        const часткаПеремог = перемог / (поразок + перемог);
        appendFileSync(
          "./тимч/вивід.txt",
`Із останіх 100 [розкид ${наймКст}-${найбКст}] (поточно ${і}):
          Частка покращень = ${
            часткаПеремог * 100
          }%
          Здатність збирати = ${
            (зібраних / спитів) * 100
          }%\n`,
        );
        // if (часткаПеремог >= 0.6) {
        //   наймКст++;
        // }

        перемог = 0;
        поразок = 0;
      }
    }
    console.log("Кінець вивію");
  }

  async човпитиПоГрі(бгал: БгалКР, наймКст: number, найбКст: number, оцінювач: (куб: Куб) => Promise<number> | number) {
    const гра = new Гра(this.розмірність);
    const куб = гра.створитиКуб();
    const раті = гра.жеребіРаті(наймКст, найбКст)
    const поворотиСторін = Гра.раті(раті)
    гра.розібрати(куб, раті)

    const оцінкаДоХодів = await оцінювач(куб);

    const найкПовороти = 
    [
      // await this.віщувати(бгал, куб),
      // перемішати([...Гра.всіПовороти, null]).slice(0, кількістьПоворотівСторони) as ВихДані,
      поворотиСторін.reverse().slice(0, кількістьПоворотівСторони).map(п => {
        return { ...п, годинниково: !п.годинниково }
      }) as ВихДані
    ][0]
    const дані: [ВхДаніКР[], ВихДаніКР[]] = [[await this.вВхДаніКР(куб.сколок())], [await this.вВихДаніКР(найкПовороти)]];

    // куб.вивід();
    // console.log(найкПоворот)
    найкПовороти.map((п) => (п ? куб.повернути(п) : null));
    const оцінкаПісляХодів = await оцінювач(куб);

    const різницяОцінок = оцінкаПісляХодів - оцінкаДоХодів;

    const найбПрогонів = 40;
    const прогонів = Math.ceil(найбПрогонів * Math.abs(різницяОцінок)) + 1;
    if (різницяОцінок < 0 || (різницяОцінок === 0 && оцінкаПісляХодів !== 1)) {
      await this.човпити(
        бгал,
        {
          многоВхДаніКР: дані[0],
          многоВихДаніКР: дані[1],
          // многоВихДаніКР: дані[1].map(
          //   (д, вк) =>
          //     this.віщуватиКР(бгал, дані[0][вк]).mul(
          //       this.вВивернутіВихДаніКР(д),
          //     ) as ВихДаніКР,
          // ),
        },
        { прогонів },
      );

      return false;
    } else {
      await this.човпити(
        бгал,
        {
          многоВхДаніКР: дані[0],
          многоВихДаніКР: дані[1],
          // многоВихДаніКР: дані[1].map((д, вк) => {
          //   const поточ = this.віщуватиКР(бгал, дані[0][вк]);
          //   return поточ.add(д.sub(поточ).mul(д));
          // }),
        },
        { прогонів: прогонів },
      );
      return true;
    }
  }

  async човпити(
    бгал: БгалКР,
    рядАбоНабірДаних:
      | { многоВхДаніКР: ВхДаніКР[]; многоВихДаніКР: ВихДаніКР[] }
      | tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>,
    волі?: { крятПоступу?: tf.CustomCallbackArgs; прогонів?: number; пакетівВПрогоні?: number },
  ) {
    const навчань = 1;
    const прогонів = волі?.прогонів ?? 5;
    const пакетівВПрогоні = волі?.пакетівВПрогоні ?? undefined;

    if (рядАбоНабірДаних instanceof tf.data.Dataset) {
      for (let i = 0; i < навчань; i++) {
        await бгал.fitDataset(рядАбоНабірДаних, {
          epochs: прогонів,
          batchesPerEpoch: пакетівВПрогоні,
          callbacks: волі?.крятПоступу,
          ...this.пострійЧовпення,
        });
        console.log(i + 1, "з", навчань);
      }
    } else {
      const злученіВхДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВхДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВхДаніКР.length, ...this.тварВхіднихДаних],
      );
      const злученіВихДаніКР = tf.tensor(
        tf.stack(рядАбоНабірДаних.многоВихДаніКР).arraySync(),
        [рядАбоНабірДаних.многоВихДаніКР.length, ...this.тварВихіднихДаних],
      );

      await бгал.fit(злученіВхДаніКР, злученіВихДаніКР, {
        epochs: прогонів,
        shuffle: true,
        batchesPerEpoch: пакетівВПрогоні,
        callbacks: волі?.крятПоступу,
        ...this.пострійЧовпення,
      });
    }
  }

  віщуватиКР(бгал: БгалКР, вхДані: ВхДаніКР): ВихДаніКР {
    const віщаКР = бгал.predict(вхДані.as5D(1, ...вхДані.shape)) as ВихДаніКР;
    if (Array.isArray(віщаКР)) throw new Error("Неочікуваний масив");
    return віщаКР.as1D();
  }

  async віщувати(бгал: БгалКР, вхДані: ВхДані): Promise<ВихДані> {
    const вихДаніКР = this.віщуватиКР(бгал, await this.вВхДаніКР(вхДані));
    // вихДаніКР.print();
    return this.зВихДанихКР(вихДаніКР);
  }

  async вВхДаніКР(дані: ВхДані) {
    //   console.log(
    //     вихДаніВВивід(
    //       tf.tensor(спитДані[0][1].flat().flat(), [1, this.розмірВихіднихДаних]),
    //     ),
    //   );
    return tf.tensor<ТварВхДаних>(
      this.даніКРСлужба.кубВДаніКР(дані),
      this.тварВхіднихДаних,
    );
  }

  async вВихДаніКР(дані: ВихДані) {
    return tf.tensor<ТварВихДаних>(
      this.даніКРСлужба.поворотиСторониВДаніКР(дані),
      this.тварВихіднихДаних,
    );
  }

  вВивернутіВихДаніКР(дані: ВихДаніКР) {
    return tf.tensor<ТварВихДаних>(
      дані.arraySync().map((д) => 1 - д),
      this.тварВихіднихДаних,
    );
  }

  async зВихДанихКР(вихДаніКР: ВихДаніКР) {
    return this.даніКРСлужба.поворотиСторониЗДанихКР(
      вихДаніКР.arraySync(),
    ) as ВихДані;
  }

  async ввізБгалу(путьПапки: string): Promise<БгалКР> {
    const бгал = await tf.loadLayersModel(`file://${путьПапки}/model.json`);

    this.укласти(бгал);

    return бгал;
  }
}
