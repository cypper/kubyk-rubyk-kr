import { ручитиИснуванняПутіПапкиОБ } from "../файловий-сустрій";
import tf from "./tf";

export type БгалКР = tf.LayersModel;

export abstract class УявнийКутийРозум<ВхДані, ВихДані, ВхДаніКР, ВихДаніКР> {
  abstract створитиБгал(): БгалКР;
  abstract створитиЧовпніДаніКР(): Promise<void>;

  abstract витягЧовпнийНабірДанихКР(
    бгал?: БгалКР,
  ): Promise<tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>>;
  abstract витягЧовпнийРядДанихКР(): Promise<{
    многоВхДаніКР: ВхДаніКР[];
    многоВихДаніКР: ВихДаніКР[];
  }>;

  abstract човпити(
    бгал: БгалКР,
    набірДаних: tf.data.Dataset<{ xs: tf.Tensor; ys: tf.Tensor }>,
    волі?: { крятПоступу?: tf.CustomCallbackArgs },
  ): void;
  abstract човпити(
    бгал: БгалКР,
    рядДаних: { многоВхДаніКР: ВхДаніКР[]; многоВихДаніКР: ВихДаніКР[] },
    волі?: { крятПоступу?: tf.CustomCallbackArgs },
  ): void;

  abstract човпитиВивійно(бгал: БгалКР): void;

  abstract віщуватиКР(бгал: БгалКР, вхДаніКР: ВхДаніКР): ВихДаніКР;
  abstract віщувати(бгал: БгалКР, вхДані: ВхДані): Promise<ВихДані>;

  abstract вВхДаніКР(вхДані: ВхДані): Promise<ВхДаніКР>;
  abstract вВихДаніКР(вихДані: ВихДані): Promise<ВихДаніКР>;
  abstract зВихДанихКР(вихДаніКР: ВихДаніКР): Promise<ВихДані>;

  async ввізБгалу(путьПапки: string) {
    return await tf.loadLayersModel(`file://${путьПапки}/model.json`);
  }

  async вивізБгалу(путьПапки: string, бгал: БгалКР): Promise<void> {
    ручитиИснуванняПутіПапкиОБ(путьПапки);
    await бгал.save(`file://${путьПапки}`);
  }
}
