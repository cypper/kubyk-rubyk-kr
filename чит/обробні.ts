export async function однобіжно<T, R>(
  iterable: Iterable<T>,
  fn: (a: T, вк: number) => Promise<R>,
): Promise<R[]> {
  const results = [];

  let вк = 0;
  for (const x of iterable) {
    results.push(await fn(x, вк));
    вк++;
  }

  return results;
}

export async function багатобіжно<T, R>(
  iterable: Iterable<T>,
  fn: (a: T) => Promise<R>,
): Promise<R[]> {
  const results = [];

  for (const x of iterable) {
    results.push(fn(x));
  }

  return Promise.all(results);
}

export function цігліСтрічки<T>(a: T[], по: (п: T) => string): T[] {
  const seen: Record<string, number> = {};
  const out: T[] = [];
  const len = a.length;
  let j = 0;
  for (let i = 0; i < len; i++) {
    const item = a[i];
    const itemValue = по(item);
    if (seen[itemValue] !== 1) {
      seen[itemValue] = 1;
      out[j++] = item;
    }
  }
  return out;
}

export function перемішати<T>(лад: T[]): T[] {
  let поточнийВк = лад.length,
    жеребийВк;

  while (поточнийВк > 0) {
    жеребийВк = Math.floor(Math.random() * поточнийВк);
    поточнийВк--;

    [лад[поточнийВк], лад[жеребийВк]] = [лад[жеребийВк], лад[поточнийВк]];
  }

  return лад;
}

export function знайтиВсіВкази<T>(
  лад: T[],
  ська: (первень: T) => boolean,
): number[] {
  const indexes = [];
  for (let i = 0; i < лад.length; i++) if (ська(лад[i])) indexes.push(i);
  return indexes;
}

export function зтяженіВипадкові<T>(
  лад: T[],
  тяжПо: (первень: T) => number,
  кількість: number,
): T[] {
  const випадковіПервні = [];
  const тяжі = лад.map(тяжПо);
  const тяжіГал = тяжі.reduce((тяжіГал, тяж, i) => {
    тяжіГал.push(тяж + (тяжіГал[i - 1] ?? 0));
    return тяжіГал;
  }, [] as number[]);

  for (let _ = 0; _ < кількість; _++) {
    const випадкове = Math.random() * тяжіГал[тяжіГал.length - 1];

    const випадковийВк = тяжіГал.findIndex((тяж) => тяж > випадкове);
    if (випадковийВк >= 0) {
      випадковіПервні.push(лад[випадковийВк]);

      const поточнийТяж = тяжі[випадковийВк];
      for (let i = випадковийВк; i < тяжіГал.length; i++) {
        тяжіГал[випадковийВк] -= поточнийТяж;
      }
    } else {
      throw new Error("Щось пішло не так");
    }
  }

  return випадковіПервні;
}

export function перетвар(
  current: number,
  in_min: number,
  in_max: number,
  out_min: number,
  out_max: number,
): number {
  const перетварено =
    ((current - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
  return перетварено < out_min
    ? out_min
    : перетварено > out_max
      ? out_max
      : перетварено;
}

export function покрутитиРяд<Т>(ряд: Т[], годинниково: boolean): Т[] {
  if (ряд.length <= 1) throw new Error("Ряд має містити хоча б 2 первні");

  if (!годинниково) ряд.unshift(ряд.pop() as Т);
  else ряд.push(ряд.shift() as Т);
  return ряд;
}

export function покрутитиМатицю<T>(матиця: T[][], годинниково: boolean): T[][] {
  return матиця[0].map((_, вк) => {
    if (годинниково) {
      return матиця.map((р) => р[вк]).reverse();
    } else {
      return матиця.map((р) => р[р.length - 1 - вк]);
    }
  });
}

export function всіМножиниЗєднів<П>(
  ряд: П[],
  сполучник: string,
  найбДовжЗєдня = ряд.length,
): (П | string)[][] {
  if (ряд.length === 0 || найбДовжЗєдня === 1) return [[...ряд]];

  const множиниЗєднів = [];
  for (let вк = 0; вк <= ряд.length - найбДовжЗєдня; вк++) {
    const передПідмножиниЗєднів = всіМножиниЗєднів(
      ряд.slice(0, вк),
      сполучник,
      найбДовжЗєдня - 1,
    );
    const післяПідмножиниЗєднів = всіМножиниЗєднів(
      ряд.slice(вк + найбДовжЗєдня),
      сполучник,
      найбДовжЗєдня,
    );

    for (const передПідмножина of передПідмножиниЗєднів) {
      for (const післяПідмножина of післяПідмножиниЗєднів) {
        множиниЗєднів.push([
          ...передПідмножина,
          ряд.slice(вк, вк + найбДовжЗєдня).join(сполучник),
          ...післяПідмножина,
        ]);
      }
    }
  }

  return [
    ...множиниЗєднів,
    ...всіМножиниЗєднів(ряд, сполучник, найбДовжЗєдня - 1),
  ];
}

export function* куски<T>(arr: T[], n: number) {
  for (let i = 0; i < arr.length; i += n) {
    yield arr.slice(i, i + n);
  }
}
